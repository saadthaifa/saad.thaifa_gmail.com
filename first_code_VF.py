print(f"""
Basic Python
==============
""")

#1)
S = "Saad" # associer "Saad" ("" => chaine de caractères) à la variable S

T = (10, 12, 1.5) # associer le tuple (...) à la variable T 

SS = "je m'appelle " + S + " et j'ai " + str(T[0]) + " ans" # associer chaine de caractères à la variable SS

print(SS)

L = list(T) # convertir le tuple T en une liste L (ensemble d'éléments séparés par des virgules et délimités par des crochets - tuple est non modifiable, à l'inverse d'une list)

L[1] = 22

SS = "je m'appelle " + S + " et j'ai " + str(L[1]) + " ans"

print(SS)

for e in L : # pour e = éléments dans L
    print(e) # faire apparaitre les éléments un par un
    
print(L) # faire apparaitre les éléments les uns à la suite des autres

#2)
val = "50"
print(val)
print("val")
print(f"{val}") # affichage formaté d'une chaine de caractères (ex. de val). f"{...}" (f string) => valeur 50 de val. Annuler l'effet des guillemets autour de val
print(f"{val:0<6}")
print(f"{val:0>6}")

#3)

d = {'age' : 44, 'name' : 'John', 'adresse' : {'Rue' : 'Menzeh', 'ville' : 'Rabat'}} # Associer à variable d le dictionnaire
print(d)

for e in d : # pour e dans le dictionnaire : e recoit les clés du dictionnaires (ex. age)
    print("l'élement de clé " + e + " est " + str(d[e]))  # str pour convertir entier en chaine de caractère et d[e] = 44.

#4)
def plus(x, y): #
    print(f'{x} + {y} = {x+y}') #f'..' <=> f"..." 
    res = x + y
    print(res)
plus(3, 5)

#5)

S = 'Welcome to the SIEE master'

print(S.split()) #convertir la chaine de caractères en une liste

S= "Cet exemple, parmi d'autres, peut encore servir"

L=S.split()

print(L)

SS= " ".join(L)

print(SS)


print(f"""
Project Python
==============
""")

# jeu pierre, papier, ciseaux
# l'ordinateur joue au hasard
from random import randint

def ecrire(nombre):
    if nombre == 1:
        print("pierre",end=" ")
    elif nombre == 2:
        print("papier",end=" ")
    else:
        print("ciseaux",end=" ")

def augmenter_scores(mon_coup,ton_coup):
    global mon_score, ton_score # global pour dire que les variables sont créées une seule fois pour être utiisé après la définition augmente scores
    if mon_coup == 1 and ton_coup == 2:
        ton_score += 1
    elif mon_coup == 2 and ton_coup == 1:
        mon_score += 1
    elif mon_coup == 1 and ton_coup == 3:
        mon_score += 1
    elif mon_coup == 3 and ton_coup == 1:
        ton_score += 1
    elif mon_coup == 3 and ton_coup == 2:
        mon_score += 1
    elif mon_coup == 2 and ton_coup == 3:
        ton_score += 1
 
ton_score = 0 #
mon_score = 0 # 
fin = 5

print("Pierre-papier-ciseaux. Le premier à",fin,"a gagné !")

while mon_score < fin and ton_score < fin: # pour assurer la répétition des coups jusqu'à ce qu'un des joueurs arrive au score "fin"
    ton_coup = int(input("1 : pierre, 2 : papier, 3 : ciseaux ? ")) # int () fonction int pour convertir une chaine de caractère saisie par l'utilisateur sous forme d'un entier
    while ton_coup < 1 or ton_coup > 3: # pour faire la gestion des erreurs de saisie
        ton_coup =int(input("1 : pierre, 2 : papier, 3 : ciseaux ? "))
    print("Vous montrez",end=" ")# end pour afficher espace après "vous montrez"
    ecrire(ton_coup) # coup de l'utilisateur
    mon_coup = randint(1,3) # mon_coup : celui de l'ordinateur
    print("- Je montre",end=" ")
    ecrire(mon_coup)
    print() # aller à la ligne
    augmenter_scores(mon_coup,ton_coup)
    print("vous",ton_score," moi",mon_score) 

